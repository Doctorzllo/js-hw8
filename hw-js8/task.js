const toDoInput = document.querySelector('input')
let warning = document.body.querySelector('.invicible')

toDoInput.addEventListener('blur', function (){
    if (toDoInput.value < 0){
        warning.className = "visible"
        toDoInput.className = "errorBorder"

        return
    } else {
        warning.className = "invicible"
        toDoInput.className = "normalBorder"
    }
    let span = document.createElement("span")
    let btn = document.createElement('button')
    let div = document.createElement('div')

    btn.innerText = 'X'
    span.innerText = toDoInput.value
    btn.addEventListener('click', function (){
        div.remove()
    })
    div.append(span,btn)
    document.body.append(div)

    toDoInput.value = ''
})


